
export enum Role {
    User = 'ROLE_user',
    Admin = 'ROLE_admin'
}