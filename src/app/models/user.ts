import { Role } from "./role";

export class User{
    id: number;
    username: string;
    fname: string;
    lname: string;
    mode: string;
    password: string;
    role: Role;
    token?: string;
}