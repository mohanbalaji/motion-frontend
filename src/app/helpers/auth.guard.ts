import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';


@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser) {
            // check if route is restricted by role
            if (route.data.roles && route.data.roles.indexOf('ROLE_admin') === -1) {
                // role not authorised so redirect to home page
                console.log(route.data.roles.length);
                console.log(route.data.roles + " index " + route.data.roles.indexOf(currentUser.role) + " role " + currentUser.role);
                console.log("not authorised");
                this.router.navigate(['/']);

                return false;
            }

            console.log("authorised");

            // authorised so return true
            return true;
        }

        console.log("not logged in");

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}